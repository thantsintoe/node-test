const { respond, ensureAuthenticated } = require('../components/util')
const passport = require('passport')
const passportService = require('../services/passport')
const authentication = require('../components/authentication')

const requireAuth = passport.authenticate('jwt', { session: false })
const requireSignin = passport.authenticate('local', { session: false })

module.exports = app => {
  app.post('/api/signin', requireSignin, authentication.signin)
  app.post('/api/signup', authentication.signup)
}
