const { respond } = require('../components/util')
const orderComponent = require('../components/order')
const passport = require('passport')
const requireAuth = passport.authenticate('jwt', { session: false })

module.exports = app => {
  app.post('/api/order', requireAuth, orderComponent.processOrder, respond)
  app.get('/api/order', requireAuth, orderComponent.getAllOrders, respond)
  app.get('/api/order/:id', orderComponent.getOrderById, respond)
}
