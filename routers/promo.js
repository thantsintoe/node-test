const { respond } = require('../components/util')
const promoComponent = require('../components/promo')
const passport = require('passport')
const { isAdmin } = require('../components/util')
const requireAuth = passport.authenticate('jwt', { session: false })

module.exports = app => {
  app.post(
    '/api/promo',
    requireAuth,
    isAdmin,
    promoComponent.createPromo,
    respond
  )
  app.get('/api/promo', requireAuth, promoComponent.getAllPromos, respond)
}
