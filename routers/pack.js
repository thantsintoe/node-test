const { respond } = require('../components/util')
const packComponent = require('../components/pack')
const passport = require('passport')
const { isAdmin } = require('../components/util')
const requireAuth = passport.authenticate('jwt', { session: false })

module.exports = app => {
  app.post('/api/pack', requireAuth, isAdmin, packComponent.createPack, respond)
  app.get('/api/pack', requireAuth, packComponent.getAllPacks, respond)
  app.get('/api/pack/:id', requireAuth, packComponent.getPackById, respond)
}
