const User = require('../models/user')
const jwt = require('jwt-simple')
const CONFIG = require('../config/config')
const { Logger } = require('./logger')
const logger = new Logger()

const generateTokenForUser = user => {
  const timestamp = new Date().getTime()
  return jwt.encode({ sub: user._id, iat: timestamp }, CONFIG.secret)
}
exports.signup = async (req, res, next) => {
  const { username, password, email } = req.body
  // send err if no username or password provided
  if (!username || !password || !email) {
    return res
      .status(422)
      .send({ error: 'Pls provide required information to register' })
  }
  try {
    // send err if username already existed
    const existingUser = await User.findOne({ username })
    if (existingUser) {
      return res.status(422).send({ error: 'username is in use' })
    }
    // create new user
    const user = new User({
      username,
      email,
      password
    })
    // save user
    await user.save()
    logger.log(`New user created: ${user._id}`)
    return res.json({ userId: user._id, token: generateTokenForUser(user) })
  } catch (err) {
    next(err)
  }
}

exports.signin = (req, res) => {
  logger.log(`User sign in: ${req.user._id}`)
  res.send({ userId: req.user._id, token: generateTokenForUser(req.user) })
}
