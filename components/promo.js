const Promo = require('../models/promo')
const util = require('./util')
const { Logger } = require('./logger')
const logger = new Logger()

exports.createPromo = util.asyncErrorHandlerMiddleware(
  async (req, res, next) => {
    const inputObj = req.body
    try {
      const newPromo = await new Promo({ ...inputObj })
      await newPromo.save()
      logger.log('New promo code created as follow:')
      logger.log(newPromo)
      util.writeSuccessResponse(res, null, newPromo)
    } catch (e) {
      console.log(e)
      util.writeErrorResponse(
        res,
        500,
        'Error while trying to create new promocode'
      )
    }
    next()
  }
)

exports.getAllPromos = util.asyncErrorHandlerMiddleware(
  async (req, res, next) => {
    const promos = await Promo.find({})
    res.response = {
      errorCode: 0,
      message: 'Success',
      data: promos
    }
    next()
  }
)

exports.getPromoById = util.asyncErrorHandlerMiddleware(
  async (req, res, next) => {
    const promo = await Promo.findById(req.params.id)
    res.response = {
      errorCode: 0,
      message: 'Success',
      data: promo
    }
    next()
  }
)
