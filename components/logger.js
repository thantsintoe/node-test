const log4js = require('log4js')
const CONFIG = require('../config/config')
class Logger {
  constructor () {
    log4js.configure({
      appenders: {
        everything: { type: 'file', filename: './logs/main.log' }
      },
      categories: {
        default: { appenders: ['everything'], level: 'debug' }
      }
    })
    this.logger = log4js.getLogger()
    if (CONFIG.log) this.logger.level = 'debug'
  }

  log (message) {
    this.logger.debug(message)
  }

  error (message) {
    this.logger.error(message)
  }
}
module.exports.Logger = Logger
