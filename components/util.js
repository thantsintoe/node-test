const Promo = require('../models/promo')
const Pack = require('../models/pack')
const Order = require('../models/order')
const { Logger } = require('./logger')
const logger = new Logger()

exports.respond = (req, res) => {
  // todo: log or record something somewhere
  res.json(res.response)
}
const isPromoActive = promo => {
  if (promo.useCount < promo.useLimit) return true
  return false
}
const getPromoDiscountPercent = async promoId => {
  const promo = await Promo.findById(promoId)
  if (promo) {
    if (isPromoActive(promo)) return promo.discountPercent
  }
  return 0
}
const increasePromoUseCount = async promoId => {
  const promo = await Promo.findById(promoId)
  if (promo) {
    if (promo.useCount < promo.useLimit) {
      await Promo.findByIdAndUpdate(promoId, {
        useCount: promo.useCount + 1
      })
    }
  }
}
exports.isOrderValid = async order => {
  const promoDiscountPercent = await getPromoDiscountPercent(order.promoId)
  const pack = await Pack.findById(order.packId)
  if (pack) {
    const subtotal =
      pack.pack_price - pack.pack_price * (promoDiscountPercent / 100)
    const gst = pack.pack_price * 0.07
    const discountPercent = pack.discount_percent
    const grandTotal =
      subtotal + gst - (subtotal + gst) * (discountPercent / 100)
    if (grandTotal === order.grandTotal) return true
  }
  return false
}
exports.fulfilOrder = async order => {
  try {
    // implement logic for payment processing from user's account
    // update order status to 'completed'
    await Order.findByIdAndUpdate(order._id, {
      orderStatus: 'completed',
      paymentCompleted: true
    })
    // update promo code usage
    await increasePromoUseCount(order.promoId)
    logger.log(`Order ID ${order._id} is fulfilled`)
    logger.log(`Promocode ${order.promoId} is used in order ${order._id}`)
  } catch (e) {
    // if error while fulfilling order
    console.log(e)
    await Order.findByIdAndUpdate(order._id, { orderStatus: 'failed' })
  }
}

exports.asyncErrorHandlerMiddleware = fn => (req, res, next) => {
  Promise.resolve(fn(req, res, next)).catch(err => {
    console.log(err)
    return res.status(501).json({
      code: 501,
      error: err.message
    })
  })
}

exports.ensureAuthenticated = (req, res, next) => {
  if (req.isAuthenticated()) {
    return next()
  }
  res.status(401).json({
    errorCode: 401,
    message: 'unauthorize'
  })
}

exports.isAdmin = (req, res, next) => {
  if (req.isAuthenticated() && req.user.username === 'admin') {
    return next()
  }
  res.status(401).json({
    errorCode: 401,
    message: 'Admin right required.'
  })
}

exports.writeSuccessResponse = (res, successMsg, data) => {
  const responseBody = {
    errorCode: 0,
    message: 'Success'
  }
  if (successMsg) responseBody.message = successMsg
  if (data) responseBody.data = data
  res.status(200)
  res.response = responseBody
  return res
}

exports.writeErrorResponse = (res, errorCode, errorMsg) => {
  res.status(errorCode)
  res.response = {
    errorCode,
    message: errorMsg
  }
  return res
}
