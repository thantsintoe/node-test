const Order = require('../models/order')
const util = require('./util')
const { Logger } = require('./logger')
const logger = new Logger()

exports.processOrder = util.asyncErrorHandlerMiddleware(
  async (req, res, next) => {
    const inputOrder = req.body
    try {
      const isOrderValid = await util.isOrderValid(inputOrder)
      if (isOrderValid) {
        const newOrder = await new Order({
          ...inputOrder,
          orderStatus: 'processing'
        })
        await newOrder.save()
        logger.log('New order created as follow:')
        logger.log(newOrder)
        setTimeout(() => {
          util.fulfilOrder(newOrder)
        }, 1000)
        util.writeSuccessResponse(res, null, newOrder)
      } else util.writeErrorResponse(res, 422, 'Ivalid Order')
    } catch (e) {
      console.log(e)
      util.writeErrorResponse(res, 500, 'Error while trying to create pack')
    }
    next()
  }
)

exports.getAllOrders = util.asyncErrorHandlerMiddleware(
  async (req, res, next) => {
    const orders = await Order.find({})
    res.response = {
      errorCode: 0,
      message: 'Success',
      data: orders
    }
    next()
  }
)

exports.getOrderById = util.asyncErrorHandlerMiddleware(
  async (req, res, next) => {
    const order = await Order.findById(req.params.id)
    res.response = {
      errorCode: 0,
      message: 'Success',
      data: order
    }
    next()
  }
)
