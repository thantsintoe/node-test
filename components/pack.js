const Pack = require('../models/pack')
const util = require('./util')
const { Logger } = require('./logger')
const logger = new Logger()

exports.createPack = util.asyncErrorHandlerMiddleware(
  async (req, res, next) => {
    const inputPack = req.body
    try {
      const newPack = await new Pack({ ...inputPack })
      await newPack.save()
      logger.log('New pack created as follow:')
      logger.log(newPack)
      util.writeSuccessResponse(res, null, newPack)
    } catch (e) {
      console.log(e)
      util.writeErrorResponse(res, 500, 'Error while trying to create pack')
    }
    next()
  }
)

exports.getAllPacks = util.asyncErrorHandlerMiddleware(
  async (req, res, next) => {
    const packs = await Pack.find({})
    res.response = {
      errorCode: 0,
      message: 'Success',
      data: packs
    }
    next()
  }
)

exports.getPackById = util.asyncErrorHandlerMiddleware(
  async (req, res, next) => {
    const pack = await Pack.findById(req.params.id)
    res.response = { pack }
    next()
  }
)
