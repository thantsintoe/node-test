const fs = require('fs')
const http = require('http')
const https = require('https')
const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const mongoose = require('mongoose')
const cors = require('cors')
const passport = require('passport')
const swaggerJsDoc = require('swagger-jsdoc')
const swaggerUi = require('swagger-ui-express')
const CONFIG = require('./config/config')
const { seedDB } = require('./seedDB')
const authRouter = require('./routers/auth')
const packRouter = require('./routers/pack')
const promoRouter = require('./routers/promo')
const orderRouter = require('./routers/order')
const { Logger } = require('./components/logger')
const logger = new Logger()

const environment = CONFIG.environment
console.log('ENVIRONMENT: ', environment)
const app = express()
const { databaseURL } = CONFIG[environment]
mongoose.connect(databaseURL, async err => {
  if (err) {
    logger.log(err)
    process.exit()
  }
  logger.log(`Successfully connected to ${databaseURL}`)
  app.emit('databaseReady')
  // await seedDB(databaseURL)
})

// allow cross-origin request
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Credentials', true)
  res.header('Access-Control-Allow-Origin', req.headers.origin)
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
  res.header(
    'Access-Control-Allow-Headers',
    'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, token'
  )
  if (req.method === 'OPTIONS') {
    res.sendStatus(200)
  } else {
    next()
  }
})

// middlewares
app.use(cors())
// app.use(morgan('combined'))
app.use(
  bodyParser.json({
    type: '*/*'
  })
)

const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: 'Codigo Backend API',
      version: '1.0.0', // Version (required)
      description: 'API information for node.js test',
      contact: {
        name: 'mr.thantsintoe@gmail.com'
      },
      servers: [`http://localhost:${CONFIG.port.http}`]
    }
  },
  apis: ['./docs/*.yaml']
}

const swaggerDocs = swaggerJsDoc(swaggerOptions)
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs))

authRouter(app)
packRouter(app)
promoRouter(app)
orderRouter(app)

if (environment !== 'test') {
  const httpServer = http.createServer(app)
  httpServer.listen(CONFIG.port.http)
  logger.log(`HTTP Server listening at port ${CONFIG.port.http}`)
}

if (environment === 'production') {
  const privateKey = fs.readFileSync(
    `${CONFIG.certificateDirectory}/privkey.pem`,
    'utf8'
  )
  const certificate = fs.readFileSync(
    `${CONFIG.certificateDirectory}/cert.pem`,
    'utf8'
  )
  const ca = fs.readFileSync(`${CONFIG.certificateDirectory}/chain.pem`, 'utf8')
  const credentials = {
    key: privateKey,
    cert: certificate,
    ca: ca
  }
  const httpsServer = https.createServer(credentials, app)
  httpsServer.listen(CONFIG.server.https_port, err => {
    if (err) logger.log(err)
    logger.log(`Https Server listening at ${CONFIG.port.https}`)
  })
}
module.exports = app
