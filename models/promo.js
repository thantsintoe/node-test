const mongoose = require('mongoose')
const { Schema } = mongoose

const promoSchema = new Schema({
  code: {
    type: String,
    require: true
  },
  discountPercent: {
    type: Number,
    require: true,
    default: 5
  },
  useCount: {
    type: Number,
    require: true
  },
  useLimit: {
    type: Number,
    default: 5
  }
})

module.exports = mongoose.model('promo', promoSchema)
