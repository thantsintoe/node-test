const mongoose = require('mongoose')
const { Schema } = mongoose

const packSchema = new Schema({
  disp_order: {
    type: Number,
    required: true,
    unique: true
  },
  pack_name: {
    type: String,
    required: true
  },
  pack_alias: {
    type: String,
    required: true
  },
  pack_type: {
    type: String,
    required: true
  },
  pack_description: {
    type: String,
    required: true
  },
  total_credit: {
    type: Number,
    required: true
  },
  tag_name: {
    type: String
  },
  validity_month: {
    type: Number,
    required: true
  },
  pack_price: {
    type: Number,
    required: true
  },
  discount_percent: {
    type: Number,
    default: 0
  },
  estimate_price: {
    type: Number,
    required: true
  },
  newbie_first_attend: {
    type: Boolean,
    required: true
  },
  newbie_addition_credit: {
    type: Number,
    required: true
  },
  newbie_note: {
    type: String
  }
})

module.exports = mongoose.model('pack', packSchema)
