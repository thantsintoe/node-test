const mongoose = require('mongoose')
const { Schema } = mongoose

const orderSchema = new Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  packId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  promoId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Promo'
  },
  subtotal: {
    type: Number,
    require: true
  },
  gst: {
    type: Number,
    require: true
  },
  discountPercent: {
    type: Number,
    required: true
  },
  grandTotal: {
    type: Number,
    required: true
  },
  paymentCompleted: {
    type: Boolean,
    required: true
  },
  orderStatus: {
    type: String,
    required: true
  }
})

module.exports = mongoose.model('order', orderSchema)
