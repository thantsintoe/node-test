const mongoose = require('mongoose')
const Pack = require('./models/pack')
const User = require('./models/user')
const Promo = require('./models/promo')
const Order = require('./models/order')
const data = require('./data.json')

const createUser = async function (username, email, password) {
  // create new user
  const user = new User({
    username,
    email,
    password
  })
  // save user
  await user.save()
  return user
}
const createNewPack = async function (dataObj) {
  const pack = new Pack(dataObj)
  await pack.save()
  return pack
}

const createPromocode = async function (codeObj) {
  const promo = new Promo(codeObj)
  await promo.save()
  return promo
}

module.exports.seedDB = async function (databaseURL, sampleData) {
  try {
    console.log('seeding database...')
    try {
      // await mongoose.connect(databaseURL)
      await User.collection.drop()
      await Pack.collection.drop()
      await Promo.collection.drop()
      await Order.collection.drop()
    } catch (e) {
      console.log('Error while dropping collections')
    }
    const testUser = await createUser(
      'thantsintoe',
      'thant@gmail.com',
      'password'
    )
    const packList = []
    const testPromo = await createPromocode({
      code: 'codigo',
      discountPercent: 10,
      useCount: 0,
      useLimit: 3
    })
    for (let i = 0; i < 5; i += 1) {
      packList.push(await createNewPack(data.pack_list[i]))
    }
  } catch (e) {
    console.error(e)
    // process.exit()
  }
}
