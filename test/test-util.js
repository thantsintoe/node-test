const { spawn } = require('child_process')

async function sleep (ms = 0) {
  return new Promise(resolve => setTimeout(resolve, ms))
}

async function startServer (outputStream) {
  console.log('starting test server...')
  try {
    var env = Object.create(process.env)
    env.NODE_ENV = 'test'
    const server = spawn('node', ['server.js'], { env: env })
    if (outputStream) {
      server.stdout.on('data', chunck => {
        console.log(`${chunck.toString()}`)
      })
      server.stderr.on('data', chunck => {
        console.log(`${chunck.toString()}`)
      })
    }
  } catch (e) {
    console.log(e)
  }
}

module.exports.startServer = startServer
module.exports.sleep = sleep
