const app = require('../../../server')
const supertest = require('supertest')
const { expect } = require('chai')
const mongoose = require('mongoose')
const User = require('../../../models/user')
const Pack = require('../../../models/pack')
const data = require('../../../data.json')
const agent = supertest(app)
const config = require('../../../config/config')

let adminToken

before(function (done) {
  this.timeout(20000)
  if (config.environment !== 'test') {
    console.log('Environment needs to be setup as test')
    process.exit(0)
  }
  app.on('databaseReady', async function () {
    console.log('Database Ready...')
    try {
      await User.collection.drop()
      await Pack.collection.drop()
    } catch (e) {
      console.log('Error while cleaning database')
    }
    const createdUser = await User.create({
      username: 'tester1',
      email: 'tester@gmail.com',
      password: 'tester'
    })
    await createdUser.save()
    agent
      .post('/api/signup')
      .send({
        username: 'admin',
        email: 'admin@gmail.com',
        password: 'password'
      })
      .expect(200)
      .expect(res => {
        expect(res.body).to.have.property('token')
        adminToken = res.body.token || null
      })
      .end(done)
  })
})

describe('Create a new pack', function () {
  this.timeout(10000)

  it('create a new pack for valid input', done => {
    agent
      .post('/api/pack')
      .set('token', adminToken)
      .send(data.pack_list[0])
      .expect(200)
      .expect(res => {
        expect(res.body.message).to.equal('Success')
      })
      .end(done)
  })

  it('NOT create a product for non-admin users', done => {
    agent
      .post('/api/signup')
      .send({
        username: 'tester2',
        email: 'teser2@gmail.com',
        password: 'tester'
      })
      .expect(200)
      .end((err, res) => {
        if (err) console.log(err)
        const testerToken = res.body.token
        agent
          .post('/api/pack')
          .set('token', testerToken)
          .send(data.pack_list[1])
          .expect(401)
          .end((err, res) => {
            if (err) console.log(err)
            done()
          })
      })
  })
})

describe('Get all packs', function () {
  this.timeout(10000)
  it('Should return packs for authenticated user', done => {
    agent
      .post('/api/signin')
      .send({
        username: 'tester2',
        password: 'tester'
      })
      .expect(200)
      .end((err, res) => {
        if (err) console.log(err)
        const testerToken = res.body.token
        agent
          .get('/api/pack')
          .set('token', testerToken)
          .expect(200)
          .end((err, res) => {
            if (err) console.log(err)
            expect(res.body.message).to.equal('Success')
            done()
          })
      })
  })
  it('NOT return packs for unauthenticated users', done => {
    agent
      .get('/api/pack')
      .expect(401)
      .end(done)
  })
  after(() => {
    mongoose.connection.close()
  })
})
