const app = require('../../../server')
const request = require('supertest')
const { expect } = require('chai')
const User = require('../../../models/user')
const mongoose = require('mongoose')
const agent = request(app)
let token

before(function (done) {
  this.timeout(20000)
  app.on('databaseReady', async function () {
    console.log('Database Ready...')
    try {
      await User.collection.drop()
      done()
    } catch (e) {
      console.log('Error while cleaning user database')
      done()
    }
  })
})

describe('Auth Route Handler', function () {
  this.timeout(20000)
  it('should sign up user', done => {
    agent
      .post('/api/signup')
      .send({
        username: 'harry',
        email: 'harry@gmail.com',
        password: 'expectopatronum'
      })
      .expect(200)
      .expect(res => {
        expect(res.body).to.have.property('token')
        token = res.body.token || null
      })
      .end(done)
  })
  it('should sign in and get token for thantsintoe', done => {
    agent
      .post('/api/signin')
      .send({
        username: 'harry',
        password: 'expectopatronum'
      })
      .expect(200)
      .expect(res => {
        expect(res.body).to.have.property('token')
      })
      .end(done)
  })

  it('should not sign in for wrong user/password', done => {
    agent
      .post('/api/signin')
      .send({
        username: 'volde',
        password: 'wrongpassword'
      })
      .expect(401)
      .expect(res => {
        expect(res.text).to.equal('Unauthorized')
      })
      .end(done)
  })

  it('should not sign in for empty fields', done => {
    agent
      .post('/api/signin')
      .send({})
      .expect(400)
      .expect(res => {
        expect(res.text).to.equal('Bad Request')
      })
      .end(done)
  })
})

after(() => {
  mongoose.connection.close()
})
