const passport = require('passport')
const User = require('../models/user')
const LocalStrategy = require('passport-local')
const JwtStragety = require('passport-jwt').Strategy
const { ExtractJwt } = require('passport-jwt')
const CONFIG = require('../config/config')

// Local Stragety Setup
const localOption = { usernameField: 'username' }
const localLogin = new LocalStrategy(
  localOption,
  (username, password, done) => {
    // verify username and password and call 'done' if correct
    User.findOne({ username }, (err, user) => {
      if (err) {
        return done(err)
      }
      if (!user) {
        return done(null, false)
      }
      user.comparePassword(password, (error, isMatch) => {
        if (error) {
          return done(error)
        }
        if (!isMatch) {
          return done(null, false)
        }
        return done(null, user)
      })
    })
  }
)

// JWT Strategy setup
const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromHeader('token'),
  secretOrKey: CONFIG.secret
}

const jwtLoginStrategy = new JwtStragety(jwtOptions, (payload, done) => {
  // check if user id in payload exist in database
  User.findById(payload.sub, (err, user) => {
    if (err) {
      return done(err, false)
    }
    if (user) {
      done(null, user)
    } else {
      done(null, false)
    }
  })
})

passport.use(jwtLoginStrategy)
passport.use(localLogin)
